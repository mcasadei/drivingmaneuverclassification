# Driving Maneuver Classification via Statistically Extracted Features

This project provides a full-fledged Python application to execute statistical feature extraction from the UAH DriveSet [[2]](#2) and then classify driving maneuvers via a *Random Forest Classifier*.

This work is based on the experiments presented in [[1]](#1), exploiting the *UAH DriveSet* described in [[2]](#2) and available for download [here](http://www.robesafe.uah.es/personal/eduardo.romera/uah-driveset/).


## Requirements 

The application needs **python 3** installed: **pip** is adopted to install all the required dependencies. The project has been successfully tested on **Ubuntu 14.04.5 LTS** and **python 3.6**, leveraging on **conda environments**.


## Installation

Installation is pretty straightforward, just follow the steps below on your command-line shell.

* Make sure current directory corresponds with project main directory ```DrivingManeuverClassification```, then type:

```bash 
make
``` 

This will install all the required python dependencies, i.e the python modules necessary for the application to work (as specified in [requirements.txt](requirements.txt), as well as create the necessary folder structure. Two folders will be created:

* ```generated_data```, hosting the data resulting from the preprocessing stage (in ```.csv``` format) such as preprocessed raw data, windowed-feature and windowed-label files for each window size to be considered in classification experiments.

* ```results```, containing the score data resulting from classification experiments, such as validation via leave-one-driver out strategy and classifications on the test set: this folder hosts results in ```.csv``` format along with a comprehensive set of plots in ```.html``` and ```.png``` formats (relying on [Bokeh](https://bokeh.pydata.org/en/latest/) and [Matplotlib](https://matplotlib.org/) respectively).

Preprocessing, data preparation and selection rely on [Pandas](https://pandas.pydata.org/) and [NumPy](http://www.numpy.org/), while classification via Random Fores is based on [Scikit-learn](http://scikit-learn.org/stable/).


## Execution

To execute application just type:

```bash
make run
```

or, alternatively:

```python
python -m mc.drivingmaneuver.app
```

This will start script execution: execution will go through the different stages of feature extraction and classification from the raw data in ```UAH-DRIVESET-v1```:

* *raw data preprocessing*, doing *resampling* and *linear interpolation* into a fixed sample rate of *10 Hz* and applying a *Kalman filter* for noise reduction. **This process is repeated** for each of the **39 driving sessions** available on the UAH-DriveSet, as such it **may take a while to complete**. 

* *windowing* so as to split the signals in different *non-overlapping windows*: according to [[1]](#1) several window sizes have been tested (0.4, 1, 1.6, 2.2, 2.8, 3.4, 4, 4.6, 5.2 and 5.8 s). The windowing phase is then repeated for each window size to be evaluated during subsequent classification process.

* *Extraction of statistical features* and generation of the train and test datasets: this produces a series of files ```extracted_features_[X]_sec.csv``` (where [X] is to be replaced with the window size) saved in the ```generated_data/``` directory. 

* *Classification*: this is done via exploitation of a **Random Forest Classifier** with **150 estimators** according to the method presented in [[1]](#1). Classification can be logically organized in the following steps:

  * a validation stage, where the training set is adopted for validating the classifier by a **leave-one-driver-out strategy**, producing 6 different predictions (one for each of the drivers) that will be the basis for subsequent analyses.
  
  * A testing stage, where the classifier is going to be fit on the whole training set and perfomance assessed on the test set. 
  
* The outcome of the aforementioned stages is a series of csv files in the ```results``` directory featuring a set of comprehensive results of the classification process.

* These files are the basis for the subsequent plotting stage, where several charts showing the results of classification are generated and stored in ```results```.

The whole raw-data preprocessing and feature extraction are computationally intensive tasks so, dependending on your environment and hardware, they may take quite a while to finish.

A few parameters can be configured, e.g. the *set of window sizes* adopted for running the experiments, the *number of estimators* to be used in the Random Forest Classifier, as well as the *number of runs executed on the test set*. In order to modify these parameters refer to python script [mc/drivingmaneuver/params.py](mc/drivingmaneuver/params.py).


## Additional Notes

Once a complete execution of the application is terminated, subsequent runs will take advantage of the data previously preprocessed (```generated_data``` folder) in order to spare time: accordingly, in case you needed to start preprocessing over, just delete every file present ```generated_data```. 

Along the same line, after the first execution of classification, subsequent runs will exploit the score data files (```csv``` format) in ```results``` directory as a means to avoid running classification again.
In this case, to rerun classification, just delete every ```csv``` file in ```results``` folder.


## References

### 1
J. Xie, A. R. Hilal and D. Kulić, [Driving Maneuver Classification: A Comparison of Feature Extraction Methods](https://ieeexplore.ieee.org/document/8166755/), in IEEE Sensors Journal, vol. 18, no. 12, pp. 4777-4784, June15, 15 2018.
doi: 10.1109/JSEN.2017.2780089

### 2
E. Romera, L. M. Bergasa and R. Arroyo, [Need data for driver behaviour analysis? Presenting the public UAH-DriveSet](https://ieeexplore.ieee.org/document/7795584/), 2016 IEEE 19th International Conference on Intelligent Transportation Systems (ITSC), Rio de Janeiro, 2016, pp. 387-392.
doi: 10.1109/ITSC.2016.7795584

