init: create-dirs install-deps

install-deps:
	pip install -r requirements.txt

run: create-dirs
	python -m mc.drivingmaneuver.app

create-dirs:
	mkdir -p ./generated_data
	mkdir -p ./results

clean:
	rm -rf ./generated_data
	rm -rf ./results
