import os
import re
from collections import defaultdict
import pandas as pd
from datetime import datetime
import time
from mc.drivingmaneuver import params

'''
Provides needed config params and function  to prepare 
the driveset for preprocessing, and feature extraction
'''

DRIVESTYLES = ["normal", "aggressive", "drowsy"]
RAW_COL_NAMES = {2: 'acc_x', 3: 'acc_y', 4: 'acc_z', 8: 'roll', 9: 'pitch', 10: 'yaw', 1: 'lin_speed'}
EVT_COL_NAMES = {1: 'event'}


drivers = defaultdict(list)

def load_configuration():
    '''
    Load the details of the whole UAH driveset that is going to be used for the project.
    :return:
    '''
    for root, dirs, files in os.walk(params.DRIVESET_PATH):
        for d in dirs:
            m = re.match(r"^(\d+)-(\d+km)-(D\d+)-(\w*)-(\w*)$", d)
            drivers[m.group(3)].append((m.group(1), m.group(2), m.group(4), d))


def range_uah_driveset():
    '''
    This is a generator ranging over the complete UAH Dataset: returning each time a pandas dataframe for
    each test (motorway) drive available.
    :return:
    '''
    if len(drivers) == 0:
        load_configuration()
    for d in drivers:
        for test in drivers[d]:
            try:
                yield load_driveset(d, test[3])
            except Exception as e:
                print("error occurred while parsing a driveset " + e)



def load_driveset(driver=None, name=None):
    '''
    Load a complete driveset test

    :param name: name of the driveset test
    :return: a tuple (a, e) containing the Pandas dataframe correspoding to raw measurements and registered inertial
            events (those we are interested in classifying via machine learning)
    '''
    print("{}".format(name))
    if driver is None or driver not in drivers:
        raise ValueError("You must select a driver among the one available: {}".format(drivers.keys))
    if name is None:
        raise ValueError("Please select a valid driveset test among those available:")

    drivesets = drivers[driver]
    found_dataset = None
    timestamp = None
    for (ts, _, s, folder) in drivesets:
        if folder.lower() == name.lower():
            found_dataset = folder
            timestamp = ts

    def _parse_date(eleapsed_seconds, start_date=timestamp):
        base_date = datetime.strptime(start_date, '%Y%m%d%H%M%S')
        tot_seconds = time.mktime(base_date.timetuple()) + float(eleapsed_seconds)
        return datetime.fromtimestamp(tot_seconds)

    if found_dataset:
        raw_gps = pd.read_csv(os.path.join(params.DRIVESET_PATH, found_dataset, params.RAW_GPS_FILENAME), date_parser=_parse_date,
                              parse_dates=[0], index_col=0, delim_whitespace=True, header=None)
        raw_acc = pd.read_csv(os.path.join(params.DRIVESET_PATH, found_dataset, params.RAW_ACC_FILENAME), date_parser=_parse_date,
                              parse_dates=[0], index_col=0, delim_whitespace=True, header=None)
        events = pd.read_csv(os.path.join(params.DRIVESET_PATH, found_dataset, params.EVENTS_FILENAME), date_parser=_parse_date,
                             parse_dates=[0], index_col=0, delim_whitespace=True, header=None)

        #only columns containing measures of interest are actually returned
        return driver, raw_acc[[2, 3, 4, 8, 9, 10]].rename(columns=RAW_COL_NAMES).sort_index(), raw_gps[[1]].rename(columns=RAW_COL_NAMES).sort_index(), events.rename(columns=EVT_COL_NAMES).sort_index()
    else:
        raise KeyError("Impossible to find a dataset for driver {} - {}".format(driver, name))


# setting up configuration
load_configuration()

if __name__ == "__main__":
    g, a, e = load_driveset("D1", "DROWSY")
    print(g)
