import os.path
PARAMS = {
    "sampling_interval": "100L", #corresponds to 10 Hz: this is the resampling interval (100 ms)
    "window_size": [0.4, 1, 1.6, 2.2, 2.8, 3.4, 4, 4.6, 5.2, 5.8], #windows size to consider in seconds
    "no_iterations": 10, #no of  random forest runs on the test set
    "preprocessed_filename": 'driveset-preprocessed.csv',
    "generated_labels_filename": 'labels_{}_sec.csv',
    "extracted_features_filename": 'extracted_features_{}_sec.csv',
    "classification_result_filename": 'final_results-f1_scores.csv',
    "classification_test-result_filename": 'final_test-results-f1_scores.csv',
    "confusion_matrix-test_result_filename": 'confusion_matrix_test.csv',
    "confusion_matrix_result_filename": 'confusion_matrix_validation.csv',
    "feature_gen_input_cols": ['acc_x', 'acc_y', 'acc_z', 'roll', 'pitch', 'yaw', 'roll_speed', 'pitch_speed', 'yaw_speed', 'lin_speed']
}

CLASSES = {
    0: 'No event',
    1: 'Braking',
    2: 'Turning',
    3: 'Acceleration'
}

RANDOM_FOREST_CLASSIFIER = {
    "n_estimators": 150
}

DRIVESET_PATH = os.path.abspath("./UAH-DRIVESET-v1")
CACHE_PATH = os.path.abspath("./generated_data")
RESULT_PATH = os.path.abspath("./results")
EVENTS_FILENAME = 'EVENTS_INERTIAL.txt'
RAW_GPS_FILENAME = 'RAW_GPS.txt'
RAW_ACC_FILENAME = 'RAW_ACCELEROMETERS.txt'