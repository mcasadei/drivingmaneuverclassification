import pandas as pd
import numpy as np
from pykalman import KalmanFilter
import math
from datetime import datetime


class Preprocessor():
    '''
    Takes care of data preprocessing.
    '''
    def __init__(self, resampling_interval="100L"):
        '''
        :param resampling_interval: data will be resampled according to the specified interval: here 100L stands for
               100 ms (10 Hz)
        '''
        self.resampling_interval = resampling_interval

    def preprocess(self, driver=None, sim_id=None, dataframe_acc=None, dataframe_speed=None):
        '''
        This does the whole preprocessing work.

        :param dataframe_acc:
        :param dataframe_speed:
        :return:
        '''
        if dataframe_acc is None or dataframe_speed is None or driver is None or sim_id is None:
            pass
        else:
            unit_converter = UnitConverter()
            data_generator = AdditionalDtataGenerator(dataframe_acc)
            dataframe_acc = unit_converter.convert_acc_g_to_ms2(dataframe_acc, columns=['acc_x', 'acc_y', 'acc_z'])
            dataframe_acc = unit_converter.convert_angle_deg_to_rad(dataframe_acc, columns=['roll', 'pitch', 'yaw'])
            dataframe_acc = data_generator.calc_angular_speed(dataframe_acc, columns=['roll', 'pitch', 'yaw'])
            dataframe = self.linear_interpolation(dataframe_acc, dataframe_speed)
            dataframe = self.kalman_filter(dataframe)
            dataframe['driver'] = [driver for i in range(0, dataframe.shape[0])]
            dataframe['sim_id'] = [sim_id for i in range(0, dataframe.shape[0])]
            return dataframe

    def kalman_filter(self, dataframe=None):
        if dataframe is None:
            pass
        else:
            for c in dataframe.columns:
                kf = KalmanFilter(transition_matrices = [1],
                                  observation_matrices = [1],
                                  initial_state_mean = dataframe[c].values[0],
                                  initial_state_covariance = 1,
                                  observation_covariance = 1,
                                  transition_covariance = .01)
                filtered, _ = kf.filter(dataframe[c])
                dataframe[c] = filtered.flatten()
        return dataframe

    def linear_interpolation(self, dataframe_acc=None, dataframe_speed=None):
        if dataframe_acc is None or dataframe_speed is None:
            pass
        else:
            resampled_acc = self._resampling_raw(dataframe_acc)
            resampled_acc = resampled_acc.interpolate(method="linear")
            resampled_speed = self._resampling_raw(dataframe_speed)
            resampled_speed = resampled_speed.interpolate(method="linear")
            resampled = pd.merge(resampled_acc, resampled_speed, left_index=True, right_index=True)
            return resampled

    def _resampling_raw(self, dataframe=None):
        '''
        :param dataframe: the dataframe to be preprocessed
        :return: the resampled dataframe for raw data
        '''
        resampled = dataframe.resample(self.resampling_interval).last()
        return resampled

class AdditionalDtataGenerator():
    '''
    This is useful since we do not seem to have info on angular Speed: deriving those by looking at
    pitch, roll and jaw, and time delta: so at time t_1 angular speed on x would be:
    roll_0 - roll_1 (t_1 - t_0).
    '''
    def __init__(self, dataframe):
        basetime = dataframe.index.to_series()
        self.timedeltas = (basetime - dataframe.index.to_series().shift(1).fillna(basetime[0])).values.view('<i8') / 10 ** 9

    def calc_angular_speed(self, dataframe=None, columns=[]):
        if dataframe is None:
            pass
        elif len(columns) == 0:
            pass
        else:
            for col in columns:
                dataframe[str(col) + '_speed'] = (dataframe[col] - dataframe[col].shift(1).fillna(0)) / self.timedeltas
                dataframe.loc[dataframe.index.values[0], str(col) + '_speed'] = dataframe.iloc[1][str(col) + '_speed']
                dataframe[str(col) + '_speed'] = dataframe[str(col) + '_speed'].replace(np.inf, 0).replace(-np.inf, 0)
        return dataframe

class UnitConverter():
    '''
    Needed to convert sensed data to the appropriate unit: acceleration comes in Gs, we need it in m/sec^2.

    Angles comes in degrees while we need these in radians.
    '''
    def __init__(self):
        pass

    def convert_acc_g_to_ms2(self, dataframe=None, columns=[]):
        '''
        convert an acceleration in g to one in m/sec^2
        :param dataframe:
        :param columns:
        :return:
        '''
        if dataframe is None:
            pass
        elif len(columns) == 0:
            pass
        else:
            dataframe[columns] = dataframe[columns] * 9.80665
        return dataframe

    def convert_angle_deg_to_rad(self, dataframe=None, columns=[]):
        '''
        convert an angle in degree to one in radians
        :param dataframe:
        :param columns:
        :return:
        '''
        if dataframe is None:
            pass
        elif len(columns) == 0:
            pass
        else:
            dataframe[columns] = dataframe[columns] * np.pi / 180
        return dataframe

class WindowManager():
    '''
    This class is going to manage every detail regarding windowing of data
    '''

    def __init__(self, timeseries=None, window_size=3.4, theta=0.8):
        '''
        :param timeseries: the timeseries corresponding to the driveset to window
        :param window_size: the size of the window in sec
        :param theta: the percentage of windows size that must be "into" the event to consider the event belonging to
               the window itself
        '''
        if timeseries is None:
            raise ValueError("It is mandatory to pass a valid timeseries")
        delta = timeseries[-1] - timeseries[0]
        self.theta = theta
        self.w = pd.Timedelta('{} seconds'.format(window_size))
        self.num_windows = math.ceil(delta / self.w)
        cut = pd.cut(timeseries, self.num_windows, labels=False, retbins=True, right=False)[1]
        self.window_ranges = l = np.array([(s, e) for (s, e) in zip(cut, cut[1:])])
        self.labels = np.zeros(self.num_windows, dtype=np.int)

    def generate_labels_for_windows(self, events=None):
        if events is None:
            pass
        else:
            curr_window_index = 0
            curr_window_range = self.window_ranges[curr_window_index]
            for (ts, event) in zip(events.index, events['label'].values):
                w_s_ts = curr_window_range[0]
                w_e_ts = curr_window_range[1]
                while not(ts.value >= w_s_ts and ts.value < w_e_ts):
                    curr_window_index += 1
                    curr_window_range = self.window_ranges[curr_window_index]
                    w_s_ts = curr_window_range[0]#datetime.utcfromtimestamp(curr_window_range[0] // 1000000000)
                    w_e_ts = curr_window_range[1]#datetime.utcfromtimestamp(curr_window_range[1] // 1000000000)

                window_in = (self.w.view('<i8') * self.theta)
                if w_e_ts - ts.value >= window_in\
                    or curr_window_index == self.labels.shape[0] - 1:
                    #if self.labels[curr_window_index] > 0:
                    #    self.labels[curr_window_index - 1] = self.labels[curr_window_index]
                    self.labels[curr_window_index] = event

                else:
                    #if self.labels[curr_window_index + 1] > 0:
                    #    self.labels[curr_window_index] = self.labels[curr_window_index + 1]
                    self.labels[curr_window_index + 1] = event

        return self.labels




