from sklearn import base
import pandas as pd
import numpy as np
import math
import os.path
from mc.drivingmaneuver import params

class ColumnSelectorTransformer(base.BaseEstimator, base.TransformerMixin):

    '''
    given a pandas dataframe in input tranform it, selecting only the specified columns
    '''
    def __init__(self, col_to_skip):
        self.skip_colums = col_to_skip

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        original_columns = X.columns.values
        output_columns = []
        for col in original_columns:
            if col not in self.skip_colums:
                output_columns.append(col)
        return X[output_columns].values

class FeatureExtractorTransformer(base.BaseEstimator, base.TransformerMixin):

    def __init__(self, count, window_size=3.4):
        self.window_size = window_size
        self.count = count

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        timeseries = X.index.to_series()
        delta = timeseries[-1] - timeseries[0]
        self.w = pd.Timedelta('{} seconds'.format(self.window_size))
        num_windows = math.ceil(delta / self.w)

        cut = pd.cut(timeseries, num_windows, labels=False, retbins=True, right=False)[0]
        df = X.copy()
        df['cuts'] = cut
        window_groups = df.groupby("cuts")
        drivers = window_groups.first()['driver']
        sim_id = window_groups.first()['sim_id']
        assert num_windows == self.count
        assert sim_id.shape[0] == self.count
        df.drop(columns=['driver'], inplace=True)
        df.drop(columns=['sim_id'], inplace=True)
        window_groups = df.groupby("cuts")
        q75 = window_groups.quantile(0.75)
        q25 = window_groups.quantile(0.25)
        iqr = q75 - q25
        iqr = iqr.add_prefix('iqr/')
        kurt = window_groups.apply(pd.DataFrame.kurt)
        kurt = kurt.add_prefix('kurt/')
        min = window_groups.min()
        max = window_groups.max()
        df_range = max - min
        max = max.add_prefix('max/')
        min = min.add_prefix('min/')
        deltas = window_groups.idxmax() - window_groups.idxmin()

        def to_secs(x):
            microsecs = x.seconds * 1000000 + x.microseconds
            if x.seconds > 60:
                microsecs = microsecs - (24 * 3600 * 1000000)
            return microsecs / 1000000

        delta_sec = deltas.applymap(to_secs)
        slope = df_range / delta_sec
        slope = slope.add_prefix('slope/')
        df_range = df_range.add_prefix('range/')
        others = window_groups.aggregate(['mean', 'var', 'std', 'skew'])
        res = pd.concat([others, min, max, slope, iqr, kurt, df_range], axis= 1)
        #devise jerk
        res["jerk"] = np.sqrt((res[('acc_x', 'mean')] ** 2) + (res[('acc_y', 'mean')] ** 2) + (res[('acc_z', 'mean')] ** 2)) / self.window_size
        res['driver'] = drivers
        res['sim_id'] = sim_id
        return res