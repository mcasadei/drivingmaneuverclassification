import math

import mc.drivingmaneuver.config as config
from mc.drivingmaneuver.preprocessing import preprocessor
import pandas as pd
import numpy as np
import os.path
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from sklearn.pipeline import Pipeline
from sklearn.model_selection import train_test_split
from sklearn.model_selection import LeaveOneGroupOut
from sklearn.ensemble import RandomForestClassifier
import sklearn.metrics as metrics
from mc.drivingmaneuver.features import transformers
from mc.drivingmaneuver import params
from collections import defaultdict, OrderedDict

from bokeh.io import show, output_file, save
from bokeh.plotting import figure
from bokeh.transform import factor_cmap, dodge
import bokeh.palettes as palettes
from bokeh.models import ColumnDataSource

import warnings
warnings.filterwarnings("ignore")

def preprocessing():
    print("#### PREPROCESSING ####\n")
    # check whether or not a copy of the preprocessed dataset already exists
    preprocessing_done = False
    preprocessed_file = os.path.join(params.CACHE_PATH, params.PARAMS['preprocessed_filename'])
    if os.path.isfile(preprocessed_file):
        print('preprocessed datafile found \'{}\' found: skip preprocessing'.format(params.PARAMS['preprocessed_filename']))
        comprehensive_set = pd.read_csv(preprocessed_file, index_col=0, parse_dates=True)
        preprocessing_done = True
    else:
        comprehensive_set = pd.DataFrame()

    # takes care of preprocessing raw data
    p = preprocessor.Preprocessor(resampling_interval=params.PARAMS["sampling_interval"])

    # needed for subsequent label generation
    #timestamps of the different test drives
    time_series_for_labels = pd.Series()
    time_series_sim_id = pd.Series()
    time_series_drivers = pd.Series()

    #these are going to maintain the comprehensive num. of events upon which generate labels
    events = pd.Series()
    sim_id = pd.Series()
    drivers = pd.Series()

    total = 0
    # for each drivetest set preprocess raw data and prepare for label extractions
    for driver, a, g, e in config.range_uah_driveset():
        if preprocessing_done:
            print("skipping...")
        else:
            print("preprocessing...")

        if not preprocessing_done:
            #preprocess the drive set
            current = p.preprocess(driver, total, a, g)
            comprehensive_set = comprehensive_set.append(current)
            current_driver = current['driver']
            current_sim_id = current['sim_id']
            time_series_for_labels = time_series_for_labels.append(current.index.to_series())
        else:
            #drive set available from the file loaded from disk
            current_driver = comprehensive_set[comprehensive_set['sim_id'] == total]['driver']
            current_sim_id = comprehensive_set[comprehensive_set['sim_id'] == total]['sim_id']
            time_series_for_labels = time_series_for_labels.append(current_driver.index.to_series())

        time_series_drivers = time_series_drivers.append(current_driver)
        time_series_sim_id = time_series_sim_id.append(current_sim_id)

        events = events.append(e['event'])
        drivers = drivers.append(pd.Series([driver for _ in e.index.to_series()]))
        sim_id = sim_id.append(pd.Series([total for _ in e.index.to_series()]))
        total += 1

    #total events annotated with corresponding driver and sim id (just a counter)
    total_events = pd.DataFrame()
    total_events['label'] = events
    total_events['driver'] = drivers.values
    total_events['sim_id'] = sim_id.values

    #timestamps of all the rawdata
    time_series_total = pd.DataFrame()
    time_series_total['ts'] = time_series_for_labels
    time_series_total['driver'] = time_series_drivers.values
    time_series_total['sim_id'] = time_series_sim_id.values

    # persisting preprocessed data
    if not preprocessing_done:
        print("persisting preprocessed data to {}".format(preprocessed_file))
        comprehensive_set.to_csv(preprocessed_file)

    print("\n#### LABEL GENERATION ####\n")
    driver_for_window = {}
    labels_for_window = {}
    sim_id_for_window = {}
    for w in params.PARAMS['window_size']:
        labels_for_window[w] = np.empty(0, dtype=np.int)
        driver_for_window[w] = np.empty(0)
        sim_id_for_window[w] = np.empty(0, dtype=np.int)

    # check if labels have already been generated
    persisted_labels_found = []
    for w in params.PARAMS['window_size']:
        labels_file_name = os.path.join(params.CACHE_PATH, params.PARAMS['generated_labels_filename'].format(w))
        if os.path.isfile(labels_file_name):
            print('labels already generated for window [{} sec] (file {})'.format(w, labels_file_name))
            df = pd.read_csv(labels_file_name)
            labels_for_window[w] = df['labels'].values
            driver_for_window[w] = df['driver'].values
            sim_id_for_window[w] = df['sim_id'].values
            persisted_labels_found.append(w)

    for w in params.PARAMS['window_size']:
        if w not in persisted_labels_found:
            print("generating labels for window [{} sec]".format(w))
            for curr in time_series_total.groupby('sim_id').groups:
                time_series_for_labels = time_series_total[time_series_total['sim_id'] == curr].index.to_series()
                events = total_events[total_events['sim_id'] == curr]
                wm = preprocessor.WindowManager(time_series_for_labels, w)
                gen_labels = wm.generate_labels_for_windows(events)
                labels_for_window[w] = np.append(labels_for_window[w], gen_labels)
                driver_for_window[w] = np.append(driver_for_window[w], np.full(gen_labels.shape, events['driver'].values[0]))
                sim_id_for_window[w] = np.append(sim_id_for_window[w], np.full(gen_labels.shape, curr))

    # persisting generated labels for the different window sizes
    for w in params.PARAMS['window_size']:
        df = pd.DataFrame({"labels": labels_for_window[w], "driver": driver_for_window[w], 'sim_id': sim_id_for_window[w]})
        if w not in persisted_labels_found:
            print("persisting windowed label file {}".format(os.path.join(params.CACHE_PATH, params.PARAMS['generated_labels_filename'].format(w))))
            df.to_csv(os.path.join(params.CACHE_PATH, params.PARAMS['generated_labels_filename'].format(w)))
        labels_for_window[w] = df

    return comprehensive_set, labels_for_window

def feature_extraction(preprocessed_data, labels_for_windows):
    # feature extraction
    print("\n#### EXTRACTING FEATURES ####\n")
    persisted_features_found = []
    feature_extracted_for_window = defaultdict(pd.DataFrame)
    for w in params.PARAMS['window_size']:
        file_name = os.path.join(params.CACHE_PATH, params.PARAMS['extracted_features_filename'].format(w))
        if os.path.isfile(file_name):
            print('features already extracted for window [{} sec] (file {})'.format(w, file_name))
            df = pd.read_csv(file_name)
            persisted_features_found.append(w)
            feature_extracted_for_window[w] = df

    for w in params.PARAMS['window_size']:
        if w not in persisted_features_found:
            print("Extraction of features for window [{} sec]...".format(w))
            labels = labels_for_windows[w]
            for curr in preprocessed_data.groupby('sim_id').groups:
                count = labels[labels['sim_id'] == curr].shape[0]
                simdata_to_process = preprocessed_data[preprocessed_data['sim_id'] == curr]
                featureExtractor = transformers.FeatureExtractorTransformer(count, window_size=w)
                feature_extracted = featureExtractor.fit_transform(simdata_to_process)
                feature_extracted_for_window[w] = pd.concat([feature_extracted_for_window[w], feature_extracted])

            print("persisting extracted windowed features file {}".format(
                os.path.join(params.CACHE_PATH, params.PARAMS['extracted_features_filename'].format(w))))

            feature_extracted_for_window[w].to_csv(
                os.path.join(params.CACHE_PATH, params.PARAMS['extracted_features_filename'].format(w)))
    return feature_extracted_for_window

def _shuffle_dataset(features, labels):
    print("shuffling dataset...")
    features = features.reset_index(drop=True)
    labels = labels.reset_index(drop=True)

    #to avoid class imbalance keep only a portion of the no events (label = 0)
    avg_events = labels[labels['labels'] > 0]['labels'].value_counts().mean()

    #a sample of labels with no events
    labels_0 = labels[labels['labels'] == 0].sample(n=int(avg_events))
    labels_others = labels[labels['labels'] > 0]

    features_0 = features.loc[labels_0.index]
    features_others = features.loc[labels_others.index]

    labels = labels_others.append(labels_0)
    features = features_others.append(features_0)

    # change row order
    labels = labels.sample(frac=1)

    # align features order
    features = features.reindex(index=labels.index.values)

    return features, labels

def train_random_forest_classifier(extracted_features_for_windows, labels_for_windows, window_size):
    #shuffling data
    extracted_features, labels = _shuffle_dataset(extracted_features_for_windows, labels_for_windows)
    #X_train = extracted_features
    #y_train = labels
    #generate train and test data
    print("Splitting dataset in train and test set...")
    X_train, X_test, y_train, y_test = train_test_split(extracted_features.copy(deep=True), labels.copy(deep=True), shuffle=False, test_size = 0.2)
    print("Training sample size: {}, test sample size: {}".format(X_train.shape[0], X_test.shape[0]))
    #leave one group out validation: group is represented by driver

    group_manager = LeaveOneGroupOut()
    groups = X_train["driver"].values

    left_out_drivers = np.empty(0, dtype=np.object)
    windows =  np.empty(0, dtype=np.float)
    windows_conf_matrix = np.empty(0, dtype=np.float)
    f1_scores = np.empty(0, dtype=np.float)
    scores = np.empty((0, len(params.CLASSES.keys())), dtype=np.float)
    conf_matrixes = np.empty((0, len(params.CLASSES.keys())), dtype=np.int)

    print("Validation on trainining data for window size [{} s]: one driver out, random forest classifier with {} estimators\n".format(window_size, params.RANDOM_FOREST_CLASSIFIER['n_estimators']))
    for tr_index, ts_index in group_manager.split(X_train.copy(deep=True), y_train.copy(deep=True), groups):
        X, X_validation = X_train.iloc[tr_index].copy(), X_train.iloc[ts_index].copy()
        y, y_validation = y_train.iloc[tr_index].copy(), y_train.iloc[ts_index].copy()
        y_validation_series = y_validation['labels'].copy()
        y, y_validation = y['labels'].copy().values, y_validation['labels'].copy().values
        print("Driver left out: {}".format(X_validation["driver"].values[0]))
        class_to_sample_ratio = y_validation_series.value_counts().reindex(params.CLASSES.keys()).sort_index(ascending=True).fillna(0) / y_validation_series.count()

        rfc = RandomForestClassifier(n_estimators=params.RANDOM_FOREST_CLASSIFIER['n_estimators'], random_state=46)
        pipeline = Pipeline([
            ('col_filter', transformers.ColumnSelectorTransformer(['driver', 'sim_id', 'cuts'])),
            ("rfc", rfc)
        ])
        try:
            print("training...")
            pipeline.fit(X.copy(), y)
            print("predicting on left-out driver...")

            y_predicted = pipeline.predict(X_validation.copy())
            curr_f1_score = metrics.f1_score(y_validation, y_predicted, average=None)
            weighted_f1_score = np.sum(curr_f1_score  * class_to_sample_ratio)

            f1_scores = np.append(f1_scores, weighted_f1_score)
            windows = np.append(windows, window_size)
            left_out_drivers = np.append(left_out_drivers, X_validation["driver"].values[0])
            print("weighted f1-score: {:.3f}, [{} sec window] [left-out driver {}]".format(weighted_f1_score, window_size, X_validation["driver"].values[0]))

            scores = np.vstack([scores, curr_f1_score])
            conf_matrix = metrics.confusion_matrix(y_validation, y_predicted)
            conf_matrixes = np.vstack([conf_matrixes, conf_matrix])
            windows_conf_matrix = np.append(windows_conf_matrix, np.full(4, window_size))
        except ValueError:
            print("simulation error, skipping...")

        #left_out_drivers[stage_index] = X_validation['driver'].values[0]        #print("conf matrix: {}sec / {}".format(window_size,  metrics.confusion_matrix(y_validation, y_predicted)))

    results_dataframe = pd.DataFrame(scores, columns=[str(i) for i in params.CLASSES.keys()])
    results_dataframe["weighted-f1-score"] = f1_scores
    results_dataframe["window_size"] = windows
    results_dataframe["left_out_driver"] = left_out_drivers

    conf_matr_dataframe = pd.DataFrame(conf_matrixes, columns=[str(i) for i in params.CLASSES.keys()])
    conf_matr_dataframe["window_size"] = windows_conf_matrix


    #testing
    print("Testing on test data: training classifier on the whole training set: {} runs will be executed".format(params.PARAMS["no_iterations"]))
    windows = np.empty(0, dtype=np.float)
    windows_conf_matrix = np.empty(0, dtype=np.float)
    f1_scores = np.empty(0, dtype=np.float)
    scores = np.empty((0, len(params.CLASSES.keys())), dtype=np.float)
    conf_matrixes = np.empty((0, len(params.CLASSES.keys())), dtype=np.int)
    for i in range(params.PARAMS["no_iterations"]):
        rfc = RandomForestClassifier(n_estimators=params.RANDOM_FOREST_CLASSIFIER['n_estimators'])
        pipeline = Pipeline([
            ('col_filter', transformers.ColumnSelectorTransformer(['driver', 'sim_id', 'cuts'])),
            ("rfc", rfc)
        ])
        try:
            # fit on the whole train set now
            pipeline.fit(X_train.copy(), y_train['labels'].copy().values)

            #predict on the test set
            y_predicted = pipeline.predict(X_test.copy())

            f1_score = metrics.f1_score(y_test['labels'].values, y_predicted, average=None)
            class_to_sample_ratio = y_test['labels'].value_counts().reindex(params.CLASSES.keys()).sort_index(ascending=True).fillna(0) / y_test['labels'].count()
            weighted_f1_score = np.sum(f1_score * class_to_sample_ratio)
            print("weighted f1-score: {:.3f}".format(weighted_f1_score))

            f1_scores = np.append(f1_scores, weighted_f1_score)
            scores = np.vstack([scores, f1_score])
            windows = np.append(windows, window_size)
            conf_matrix = metrics.confusion_matrix(y_test['labels'].values, y_predicted)
            windows_conf_matrix = np.append(windows_conf_matrix, np.full(4, window_size))
            conf_matrixes = np.vstack([conf_matrixes, conf_matrix])
        except ValueError as err:
            print("skipping due to error in selected dataset")

    test_results_dataframe = pd.DataFrame(scores, columns=[str(i) for i in params.CLASSES.keys()])
    test_results_dataframe["weighted-f1-score"] = f1_scores
    test_results_dataframe["window_size"] = windows
    conf_matr_test_df = pd.DataFrame(conf_matrixes, columns=[str(i) for i in params.CLASSES.keys()])
    conf_matr_test_df["window_size"] = windows_conf_matrix

        #acc_score = metrics.f1_score(y_test['labels'].values, y_predicted, average=None)
        #print("final test score: {}sec / {}".format(window_size, acc_score))

    return results_dataframe, conf_matr_dataframe, test_results_dataframe, conf_matr_test_df

def plot_results(df_val_f1, df_test_f1, df_val_conf_matr, df_test_conf_matr):
    print("\n#### PLOT GENERATION ####\n")
    validation_f1_score_per_window_size = np.empty(0, dtype=np.float)
    validation_confidence_f1_score_per_window_size = np.empty(0, dtype=np.float)
    test_f1_score_per_window_size = np.empty(0, dtype=np.float)
    test_confidence_f1_score_per_window_size = np.empty(0, dtype=np.float)
    window_size_val = np.empty(0, dtype=np.float)
    window_size_test = np.empty(0, dtype=np.float)

    f1_scores_per_class = defaultdict(list)
    f1_confidence_per_class = defaultdict(list)
    test_f1_scores_per_class = defaultdict(list)
    test_f1_confidence_per_class = defaultdict(list)

    for w in params.PARAMS['window_size']:
        if w in df_val_f1["window_size"].unique():
            f1_scores_per_class["windows"].append(str(w))
            f1_confidence_per_class["windows"].append(str(w))
        if w in df_test_f1["window_size"].unique():
            test_f1_scores_per_class["windows"].append(str(w))
            test_f1_confidence_per_class["windows"].append(str(w))
        for i in params.CLASSES:
            if w in df_val_f1["window_size"].unique():
                #for validation results (hold-one-driver-out)
                class_results = df_val_f1[df_val_f1["window_size"] == w][str(i)]
                avg = class_results.mean()
                std = class_results.std()
                count = class_results.count()
                confidence = 1.96 * std / math.sqrt(count)
                f1_scores_per_class[str(i)].append(avg)
                f1_confidence_per_class[str(i)].append(confidence)

            if w in df_test_f1["window_size"].unique():
                #for test results (delivered by exploiting test set)
                class_test_results = df_test_f1[df_test_f1["window_size"] == w][str(i)]
                avg = class_test_results.mean()
                std = class_test_results.std()
                count = class_test_results.count()
                confidence = 1.96 * std / math.sqrt(count)
                test_f1_scores_per_class[str(i)].append(avg)
                test_f1_confidence_per_class[str(i)].append(confidence)

        if w in df_val_f1["window_size"].unique():
            score_for_window = df_val_f1[df_val_f1["window_size"] == w]["weighted-f1-score"]
            avg = score_for_window.mean()
            std = score_for_window.std()
            count = score_for_window.count()
            confidence = 1.96 * std / math.sqrt(count)

            validation_f1_score_per_window_size = np.append(validation_f1_score_per_window_size, avg)
            validation_confidence_f1_score_per_window_size = np.append(validation_confidence_f1_score_per_window_size, confidence)
            window_size_val = np.append(window_size_val, w)

        if w in df_test_f1["window_size"].unique():
            score_for_window_test = df_test_f1[df_test_f1["window_size"] == w]["weighted-f1-score"]
            avg = score_for_window_test.mean()
            std = score_for_window_test.std()
            count = score_for_window_test.count()
            confidence = 1.96 * std / math.sqrt(count)

            test_f1_score_per_window_size = np.append(test_f1_score_per_window_size, avg)
            test_confidence_f1_score_per_window_size = np.append(test_confidence_f1_score_per_window_size, confidence)
            window_size_test = np.append(window_size_test, w)

        if w in df_val_conf_matr["window_size"].unique():
            #plot a confusion matrix
            matrix_for_w = df_val_conf_matr[df_val_conf_matr["window_size"] == w][["0", "1", "2", "3"]]
            #summing 6 matrixes: one per driver (this is the validation set)
            confidence_sum = np.zeros((4, 4), dtype=np.float)
            for i in range(matrix_for_w.shape[0] // 4):
                sums = matrix_for_w.iloc[i*4:i*4+4].sum(axis=1)
                ratio = np.divide(matrix_for_w.iloc[i*4:i*4+4].values, sums.values.reshape(-1, 1) * 4)
                ratio[ratio == np.inf] = 0
                ratio = np.nan_to_num(ratio)
                confidence_sum += np.multiply(matrix_for_w.iloc[i*4:i*4+4].values, ratio)

            f = np.vectorize(lambda x: int(round(x)))
            confidence_averaged = f(confidence_sum)
            show_avg_confusion_matrix(confidence_averaged, "Averaged confusion matrix for window size {} sec".format(w), os.path.join(params.RESULT_PATH, "avg_confusion_matrix_validation-{}sec.png".format(w)))

    windows_val = [str(s) for s in window_size_val.tolist()]
    windows_test = [str(s) for s in window_size_test.tolist()]

    show_f1_score(test_f1_score_per_window_size, test_confidence_f1_score_per_window_size, windows_test, os.path.join(params.RESULT_PATH, "test_weighted_f1_scores.html"), "Performance as a function of window size (Test set) {} runs".format(params.PARAMS["no_iterations"]))
    show_f1_score(validation_f1_score_per_window_size, validation_confidence_f1_score_per_window_size, windows_val, os.path.join(params.RESULT_PATH, "validation_weighted_f1_scores.html"), "Performance as a function of window size (Validation - one driver out)")

    show_f1_score_for_category(f1_scores_per_class, f1_confidence_per_class, os.path.join(params.RESULT_PATH, "validation_f1_scores_maneuvers.html"), "Classification F1-score of each maneuver class with different window sizes")
    show_f1_score_for_category(test_f1_scores_per_class, test_f1_confidence_per_class, os.path.join(params.RESULT_PATH, "test_f1_scores_maneuvers.html"), "(Test set) Classification F1-score of each maneuver class with different window sizes")


def show_avg_confusion_matrix(confusion_averaged, title, filename):
    print("plotting {}...".format(filename))
    output_file(filename)

    f = plt.figure(figsize=(8, 6))
    plt.imshow(confusion_averaged, interpolation='nearest', cmap=plt.cm.Blues)
    plt.title(title)
    plt.colorbar()
    plt.tight_layout()
    tick_marks = np.arange(len(set(params.CLASSES.values())))
    plt.xticks(tick_marks, list(params.CLASSES.values()), rotation=45)
    plt.yticks(tick_marks, list(params.CLASSES.values()))
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    thresh = confusion_averaged.max() / 1.5
    for i in range(confusion_averaged.shape[0]):
        for j in range(confusion_averaged.shape[1]):
            plt.text(j, i, "{:,}".format(confusion_averaged[i, j]),
                        horizontalalignment="center",
                        color="white" if confusion_averaged[i, j] > thresh else "black")

    f.savefig(filename, dpi=300, bbox_inches="tight")

def show_f1_score_for_category(dict_f1, dict_confidence, filename, title):
    print("plotting {}...".format(filename))
    output_file(filename)

    data = dict_f1


    source = ColumnDataSource(data=data)

    p = figure(x_range=data["windows"], plot_height=450, plot_width=750, title=title,
               toolbar_location=None, tools="", x_axis_label="Window size (s)", y_axis_label="F1-score")

    p.vbar(x=dodge('windows', -0.35, range=p.x_range), top='0', width=0.15, source=source,
           color=palettes.Category20[4][0], legend=params.CLASSES[0])

    # create the coordinates for the errorbars
    err_xs = []
    err_ys = []

    i = 0
    factor = 0.4
    for x, y, yerr in zip(dict_f1["windows"], dict_f1["0"], dict_confidence["0"]):
        err_xs.append((float(x) - 0.27 + factor * i, float(x) - 0.27 +  factor* i))
        err_ys.append((y - yerr, y + yerr))
        i += 1

    # plot them
    p.multi_line(err_xs, err_ys, color='gray')

    p.vbar(x=dodge('windows', -0.2, range=p.x_range), top='1', width=0.15, source=source,
           color=palettes.Category20[4][1], legend=params.CLASSES[1])

    err_xs = []
    err_ys = []

    i = 0
    for x, y, yerr in zip(dict_f1["windows"], dict_f1["1"], dict_confidence["1"]):
        err_xs.append((float(x) - 0.12 + factor * i, float(x) - 0.12 + factor * i))
        err_ys.append((y - yerr, y + yerr))
        i += 1

    # plot them
    p.multi_line(err_xs, err_ys, color='gray')

    p.vbar(x=dodge('windows', -0.05, range=p.x_range), top='2', width=0.15, source=source,
           color=palettes.Category20[4][2], legend=params.CLASSES[2])

    err_xs = []
    err_ys = []

    i = 0
    for x, y, yerr in zip(dict_f1["windows"], dict_f1["2"], dict_confidence["2"]):
        err_xs.append((float(x) + 0.03 + factor * i, float(x) + 0.03 + factor * i))
        err_ys.append((y - yerr, y + yerr))
        i += 1

    # plot them
    p.multi_line(err_xs, err_ys, color='gray')

    p.vbar(x=dodge('windows', 0.12, range=p.x_range), top='3', width=0.15, source=source,
           color=palettes.Category20[4][3], legend=params.CLASSES[3])

    err_xs = []
    err_ys = []

    i = 0
    for x, y, yerr in zip(dict_f1["windows"], dict_f1["3"], dict_confidence["3"]):
        err_xs.append((float(x) + 0.21 + factor * i, float(x) + 0.21 + factor * i))
        err_ys.append((y - yerr, y + yerr))
        i += 1

    # plot them
    p.multi_line(err_xs, err_ys, color='gray')

    p.x_range.range_padding = 0.1
    p.xgrid.grid_line_color = None
    p.legend.location = "top_right"
    p.legend.orientation = "horizontal"
    save(p)

def show_f1_score(avg, confidence, window_size, filename, title):
    print("plotting {}...".format(filename))
    output_file(filename)

    p = figure(x_range=window_size, plot_height=450, plot_width=750, title=title,
               toolbar_location=None, tools="", x_axis_label="Window size (s)", y_axis_label="Weighted F1-score")

    p.vbar(x=window_size, top=avg, width=0.9)

    # create the coordinates for the errorbars
    err_xs = []
    err_ys = []

    for x, y, yerr in zip(window_size, avg, confidence):
        err_xs.append((x, x))
        err_ys.append((y - yerr, y + yerr))

    # plot them
    p.multi_line(err_xs, err_ys, color='black')

    p.xgrid.grid_line_color = None
    p.y_range.start = 0

    save(p)
    #export_png(p, filename=filename)

def classification(extracted_features_for_window, labels_for_windows):
    print("\n#### TRAINING AND TESTING THE CLASSIFIER ####\n")
    # training, validation, testing
    validation_results = pd.DataFrame()
    validation_conf_matr = pd.DataFrame()
    test_results = pd.DataFrame()
    test_conf_matr = pd.DataFrame()

    if not os.path.isfile(os.path.join(params.RESULT_PATH, params.PARAMS['classification_result_filename'])) or \
            not os.path.isfile(
                os.path.join(params.RESULT_PATH, params.PARAMS['classification_test-result_filename'])) or \
            not os.path.isfile(os.path.join(params.RESULT_PATH, params.PARAMS['confusion_matrix_result_filename'])) or \
            not os.path.isfile(os.path.join(params.RESULT_PATH, params.PARAMS['confusion_matrix_result_filename'])):
        for w in params.PARAMS['window_size']:
            result_df, conf_df, test_df, conf_test_df = train_random_forest_classifier(extracted_features_for_window[w],
                                                                                       labels_for_windows[w], w)
            validation_results = validation_results.append(result_df)
            validation_conf_matr = validation_conf_matr.append(conf_df)
            test_results = test_results.append(test_df)
            test_conf_matr = test_conf_matr.append(conf_test_df)

        validation_results.to_csv(os.path.join(params.RESULT_PATH, params.PARAMS['classification_result_filename']))
        test_results.to_csv(os.path.join(params.RESULT_PATH, params.PARAMS['classification_test-result_filename']))
        validation_conf_matr.to_csv(
            os.path.join(params.RESULT_PATH, params.PARAMS['confusion_matrix_result_filename']))
        test_conf_matr.to_csv(
            os.path.join(params.RESULT_PATH, params.PARAMS['confusion_matrix-test_result_filename']))
    else:
        print("Skipping classification, loading previous result files...")
        print("Loading {}".format(os.path.join(params.RESULT_PATH, params.PARAMS['classification_result_filename'])))
        validation_results = pd.read_csv(
            os.path.join(params.RESULT_PATH, params.PARAMS['classification_result_filename']), index_col=0,
            parse_dates=True)
        print("Loading {}".format(os.path.join(params.RESULT_PATH, params.PARAMS['confusion_matrix_result_filename'])))
        validation_conf_matr = pd.read_csv(
            os.path.join(params.RESULT_PATH, params.PARAMS['confusion_matrix_result_filename']), index_col=0,
            parse_dates=True)
        print("Loading {}".format(os.path.join(params.RESULT_PATH, params.PARAMS['classification_test-result_filename'])))
        test_results = pd.read_csv(
            os.path.join(params.RESULT_PATH, params.PARAMS['classification_test-result_filename']), index_col=0,
            parse_dates=True)
        print("Loading {}".format(os.path.join(params.RESULT_PATH, params.PARAMS['confusion_matrix-test_result_filename'])))
        test_conf_matr = pd.read_csv(
            os.path.join(params.RESULT_PATH, params.PARAMS['confusion_matrix-test_result_filename']), index_col=0,
            parse_dates=True)

    return validation_results, test_results, validation_conf_matr, test_conf_matr

if __name__ == "__main__":
    #data preprocessing
    preprocessed_data, labels_for_windows = preprocessing()

    #feature extraction
    extracted_features_for_window = feature_extraction(preprocessed_data, labels_for_windows)

    #classification
    validation_results, test_results, validation_conf_matr, test_conf_matr = classification(extracted_features_for_window, labels_for_windows)

    #plotting results
    plot_results(validation_results, test_results, validation_conf_matr, test_conf_matr)

    print("#### PROCESS TERMINATED ####\n")


